import { Component, OnInit } from '@angular/core';

import { Currency } from './types/currency';
import { Option } from './types/option';

import { DataService } from './services/data.service';
import { SearchService } from './services/search.service';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.scss']
})
export class AppComponent implements OnInit {
  data: any = {
    pageTitleH1: '',
    pageTitleH2: '',
    description: '',
    referenceLink: '',
    usDollarValue: '',
    australianDollarValue: '',
    euroValue: '',
    celebrityList: [],
    celebrityListClone: []
  };
  keywords = '';
  currencyOpts: Currency[] = [];
  selectedCurr: Currency;
  countryOpts: Option[] = [];
  selectedCountry = '';
  orderOpts: Option[] = [
    {label: 'Rank', value: 'rank'},
    {label: 'Name', value: 'name'},
    {label: 'Age',  value: 'age'}
  ];
  selectedOrder = 'rank';
  inited = false;

  constructor(
    private dataService: DataService,
    private searchService: SearchService,
  ) {}

  ngOnInit() {
    // Get app data from data service
    this.dataService.getData()
      .subscribe(data => {
        this.formatData(data);
        this.formatCurrency(
          data,
          'usDollarValue',
          'US Dollar',
          '$',
          'USD',
          true
        );
        this.formatCurrency(
          data,
          'australianDollarValue',
          'Australian Dollar',
          '$',
          'AUD',
          false
        );
        this.formatCurrency(
          data,
          'euroValue',
          'Euro',
          '€',
          'EUR',
          false
        );
        this.formatCountries(data);
        this.updateList();
        this.inited = true;
      });
  }

  /**
   * Select currency
   *
   * @param id - currency option id
   * @param opts - currency options
   */
  selectCurr(id: string|number, opts: Currency[]) {
    const idNum = typeof id === 'string' ? parseInt(id, 10) : id;
    for (const currency of opts) {
      if (currency.id === idNum) {
        this.selectedCurr = currency;
        break;
      }
    }
    this.updateList();
  }

  /**
   * Search list by keywords
   *
   * @param keywords
   */
  search(keywords: string) {
    this.keywords = keywords;
    this.updateList();
  }

  /**
   * Select country
   *
   * @param country
   */
  selectCountry(country: string) {
    this.selectedCountry = country;
    this.updateList();
  }

  /**
   * Select order
   *
   * @param order
   */
  selectOrder(order: string) {
    this.selectedOrder = order;
    this.updateList();
  }

  /**
   * Update celebrity list based on the search criteria
   * TODO::Using data-driven form instead of component props
   */
  private updateList() {
    let updatedList = this.data.celebrityList.slice(0);

    // Update celebrity net worth value based on selected currency
    if (this.selectedCurr) {
      for (const celebrity of updatedList) {
        celebrity.netWorthConverted = celebrity.netWorth * this.selectedCurr.rate;
      }
    }

    // Filter the list against the keywords, skip the netWorth props
    // The netWorthConverted value will be used for search
    if (this.keywords) {
      updatedList = this.searchService.filter(
        updatedList,
        this.keywords,
        '',
        'netWorth'
      );
    }

    if (this.selectedCountry) {
      updatedList = this.searchService.filter(
        updatedList,
        this.selectedCountry,
        'country',
        ''
      );
    }

    this.data.celebrityListClone = this.searchService.sort(
      updatedList,
      this.selectedOrder
    );
  }

  /**
   * Format server data
   *
   * @param data - data from the server
   */
  private formatData(data: any) {
    // Iterate server data and get the props defined in the component
    for (const key in data) {
      if (this.data.hasOwnProperty(key)) {
        this.data[key] = data[key];
      }
    }

    // Clone celebrity list for filtering & sorting
    if (
      data.hasOwnProperty('celebrityList') &&
      data.celebrityList &&
      Array.isArray(data.celebrityList)
    ) {
      this.data.celebrityListClone = data.celebrityList.slice(0);
    }
  }

  /**
   * Format currency dropdown options
   *
   * @param data - data from the server
   * @param key - currency data key
   * @param label - currency label used in select
   * @param symbol - currency symbol
   * @param code - currency code
   * @param selected - selected currency option
   */
  private formatCurrency(
    data: any,
    key: string,
    label: string,
    symbol: string,
    code: string,
    selected: boolean
  ) {
    if (data.hasOwnProperty(key)) {
      const rate = parseFloat(data[key]);
      if (!isNaN(rate)) {
        const option = {
          id: this.currencyOpts.length + 1,
          label: label,
          rate: rate,
          symbol: symbol,
          code: code
        };
        this.currencyOpts.push(option);
        if (selected) {
          this.selectedCurr = option;
        }
      }
    }
  }

  /**
   * Format country dropdown options
   *
   * @param data - data from the server
   */
  private formatCountries(data: any) {
    if (
      data.hasOwnProperty('celebrityList') &&
      data.celebrityList &&
      Array.isArray(data.celebrityList)
    ) {
      let uniqCountries: string[] = [];

      // Iterate celebrity list to get unique country names
      for (const celebrity of data.celebrityList) {
        if (
          celebrity.hasOwnProperty('country') &&
          celebrity.country &&
          uniqCountries.indexOf(celebrity.country) === -1
        ) {
          uniqCountries.push(celebrity.country);
        }
      }

      if (uniqCountries.length) {
        // Sort country names Alpha ASC
        uniqCountries = this.searchService.sort(uniqCountries, '');

        // Add show all option
        this.countryOpts.push({
          label: 'Show all',
          value: ''
        });

        // Convert country names to options
        for (const uniqCountry of uniqCountries) {
          this.countryOpts.push({
            label: uniqCountry,
            value: uniqCountry
          });
        }
      }
    }
  }
}
