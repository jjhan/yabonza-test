// Http testing module and mocking controller
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { TestBed, async, ComponentFixture } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';

import { of } from 'rxjs';

import { DataService } from './services/data.service';

import { AppComponent } from './app.component';

describe('AppComponent', () => {
  let app: AppComponent;
  let fixture: ComponentFixture<AppComponent>;
  let getDataSpy: jasmine.Spy;
  let testData: any;
  let compiled: any;

  beforeEach(async(() => {
    // Create a fake DataService object with a `getData()` spy
    const dataService = jasmine.createSpyObj('DataService', ['getData']);
    // Make the spy return a synchronous Observable with the test data
    getDataSpy = dataService.getData;

    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      declarations: [
        AppComponent
      ],
      providers:    [
        { provide: DataService, useValue: dataService }
      ]
    }).compileComponents();

    fixture = TestBed.createComponent(AppComponent);
    app = fixture.debugElement.componentInstance;
    compiled = fixture.debugElement.nativeElement;
    testData = {
      'pageTitleH1': 'Technical Test',
      'pageTitleH2': 'Celebrity Rich List',
      'description': 'A list of the Top 50 Richest Celebrities of 2014',
      'referenceLink': 'http://www.therichest.com/top-lists/top-100-richest-celebrities/',
      'usDollarValue': '1',
      'australianDollarValue': '0.78',
      'euroValue': '0.92',
      'celebrityList': [
        {
          'rank': 1,
          'name': 'John Walton',
          'netWorth': 21000000000,
          'age': '68',
          'country': 'United States'
        },
        {
          'rank': 2,
          'name': 'Rupert Murdoch',
          'netWorth': 14000000000,
          'age': '48',
          'country': 'Australia'
        }
      ]
    };

    getDataSpy.and.returnValue( of(testData) );
  }));

  it('should create the app', async(() => {
    expect(app).toBeTruthy();
  }));

  it('should render title in a h1 tag', async(() => {
    app.ngOnInit();
    fixture.detectChanges();
    expect(compiled.querySelector('h1').textContent).toEqual('Technical Test');
  }));

  it('should order list by rank asc by default', async(() => {
    app.ngOnInit();
    fixture.detectChanges();
    expect(compiled.querySelector('li:first-child').textContent).toContain('John Walton');
  }));

  it('should order list by age if select changes', async(() => {
    app.ngOnInit();
    fixture.detectChanges();

    const orderSelect = compiled.querySelector('select[name="order"]');
    orderSelect.value = orderSelect.options[2].value;
    orderSelect.dispatchEvent(new Event('change'));
    fixture.detectChanges();

    expect(compiled.querySelector('li:first-child').textContent).toContain('Rupert Murdoch');
  }));

  it('should filter list by country if select changes', async(() => {
    app.ngOnInit();
    fixture.detectChanges();

    const countrySelect = compiled.querySelector('select[name="country"]');
    countrySelect.value = countrySelect.options[1].value;
    countrySelect.dispatchEvent(new Event('change'));
    fixture.detectChanges();

    const li = compiled.querySelectorAll('li');
    expect(li.length).toEqual(1);
    expect(li[0].textContent).toContain('Rupert Murdoch');
  }));

  it('should filter list by keywords if input changes', async(() => {
    app.ngOnInit();
    const keywordsInput = compiled.querySelector('input[name="keywords"]');
    keywordsInput.value = '21000000000';
    keywordsInput.dispatchEvent(new Event('keyup'));
    fixture.detectChanges();
    const li = compiled.querySelectorAll('li');
    expect(li.length).toEqual(1);
    expect(li[0].textContent).toContain('John Walton');
  }));

  it('should convert currency if select changes', async(() => {
    app.ngOnInit();
    fixture.detectChanges();

    const currencySelect = compiled.querySelector('select[name="currency"]');
    currencySelect.value = currencySelect.options[1].value;
    currencySelect.dispatchEvent(new Event('change'));
    fixture.detectChanges();

    expect(compiled.querySelector('li:first-child').textContent).toContain('16,380,000,000');
  }));

  it('should filter list against converted networkth value', async(() => {
    app.ngOnInit();
    fixture.detectChanges();

    const currencySelect = compiled.querySelector('select[name="currency"]');
    currencySelect.value = currencySelect.options[1].value;
    currencySelect.dispatchEvent(new Event('change'));
    fixture.detectChanges();

    const keywordsInput = compiled.querySelector('input[name="keywords"]');
    keywordsInput.value = '16380000000';
    keywordsInput.dispatchEvent(new Event('keyup'));
    fixture.detectChanges();

    const li = compiled.querySelectorAll('li');
    expect(li.length).toEqual(1);
    expect(li[0].textContent).toContain('16,380,000,000');
  }));
});
