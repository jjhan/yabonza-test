export class Currency {
  id: number;
  label: string;
  rate: number;
  symbol: string;
  code: string;
}
