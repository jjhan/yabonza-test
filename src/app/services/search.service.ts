import { Injectable } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class SearchService {
  /**
   * Default compare method
   *
   * @param a
   * @param b
   */
  static compareFn(a: any, b: any): number {
    if (a === b) {
      return 0;
    }
    if (a == null) {
      return 1;
    }
    if (b == null) {
      return -1;
    }
    return a > b ? 1 : -1;
  }

  constructor() { }

  /**
   * Filter array of objects by string
   *
   * @param array
   * @param filter
   * @param key - object key to filter against
   * @param keySkip - object key to skip filtering
   */
  filter(array: any[], filter: string, key: string, keySkip: string): any[] {
    if (!array || !filter) {
      return array;
    }

    return array.filter(this.filterObjVal(filter, key, keySkip));
  }

  /**
   * Sort array of objects by prop
   *
   * @param array
   * @param prop - object prop to sort against
   */
  sort(array: any[], prop: string): any[] {
    if (!array) {
      return array;
    }

    if (Array.isArray(array)) {
      return this.sortArray(array.slice(), prop);
    }

    return array;
  }

  /**
   * Filter object by value
   *
   * @param filter
   * @param key - filter against specific key
   * @param keySkip - object key to skip filtering
   */
  private filterObjVal(filter, key, keySkip) {
    return object => {
      const values = key && object.hasOwnProperty(key) ? [object[key]] : Object.values(object);
      let matched = false;

      if (keySkip && object.hasOwnProperty(keySkip) && object[keySkip]) {
        const index = values.indexOf(object[keySkip]);

        if (index > -1) {
            values.splice(index, 1);
        }
      }

      for (const value of values) {
        matched = this.filterByString(filter)(value);

        if (matched) {
          return true;
        }
      }

      return false;
    };
  }

  /**
   * Filter by string
   *
   * @param filter
   */
  private filterByString(filter: string) {
    if (filter) {
      filter = filter.toLowerCase();
    }
    return value => {
      return !filter || (value ? ('' + value).toLowerCase().indexOf(filter) !== -1 : false);
    };
  }

  /**
   * Sort array
   *
   * @param array
   * @param prop
   */
  private sortArray(array: any[], prop: string): any[] {
    const arraySorted: any[] = array.sort((a: any, b: any): number => {
      if (!prop) {
        return SearchService.compareFn(a, b);
      }

      if (a && b) {
        return SearchService.compareFn(a[prop], b[prop]);
      }

      return SearchService.compareFn(a, b);
    });

    return arraySorted;
  }
}
