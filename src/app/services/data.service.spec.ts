// Http testing module and mocking controller
import { HttpClientTestingModule, HttpTestingController } from '@angular/common/http/testing';

import { TestBed, inject } from '@angular/core/testing';
import { HttpClient } from '@angular/common/http';

import { DataService } from './data.service';

describe('DataService', () => {
  let httpClient: HttpClient;
  let httpTestingController: HttpTestingController;
  let dataService: DataService;

  beforeEach(() => {
    TestBed.configureTestingModule({
      imports: [ HttpClientTestingModule ],
      providers: [DataService]
    });

    // Inject the http, test controller, and service-under-test
    // as they will be referenced by each test.
    httpClient = TestBed.get(HttpClient);
    httpTestingController = TestBed.get(HttpTestingController);
    dataService = TestBed.get(DataService);
  });

  afterEach(() => {
    // After every test, assert that there are no more pending requests.
    httpTestingController.verify();
  });

  /// DataService method tests begin ///

  describe('#getData', () => {
    let expectedData: any;

    beforeEach(() => {
      dataService = TestBed.get(DataService);
      expectedData = {result: 'success'};
    });

    it('should return expected data (called once)', () => {

      dataService.getData().subscribe(
        data => expect(data).toEqual(expectedData, 'should return expected data'),
        fail
      );

      // DataService should have made one request to GET data from expected URL
      const req = httpTestingController.expectOne(dataService.dataUrl);
      expect(req.request.method).toEqual('GET');

      // Respond with the mock data
      req.flush(expectedData);
    });

    it('should be OK returning no data', () => {

      dataService.getData().subscribe(
        data => expect(data).toEqual([], 'should have empty data'),
        fail
      );

      const req = httpTestingController.expectOne(dataService.dataUrl);
      req.flush([]); // Respond with no data
    });

    // This service reports the error but finds a way to let the app keep going.
    it('should turn 404 into an empty data result', () => {

      dataService.getData().subscribe(
        data => expect(data).toEqual([], 'should return empty data'),
        fail
      );

      const req = httpTestingController.expectOne(dataService.dataUrl);

      // respond with a 404 and the error message in the body
      const msg = 'deliberate 404 error';
      req.flush(msg, {status: 404, statusText: 'Not Found'});
    });

    it('should return expected data (called multiple times)', () => {

      dataService.getData().subscribe();
      dataService.getData().subscribe();
      dataService.getData().subscribe(
        data => expect(data).toEqual(expectedData, 'should return expected data'),
        fail
      );

      const requests = httpTestingController.match(dataService.dataUrl);
      expect(requests.length).toEqual(3, 'calls to getData()');

      // Respond to each request with different mock  results
      requests[0].flush([]);
      requests[1].flush({result: 'fail'});
      requests[2].flush(expectedData);
    });
  });
});
