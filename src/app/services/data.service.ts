import { Injectable } from '@angular/core';
import { HttpClient, HttpHeaders } from '@angular/common/http';

import { Observable, of } from 'rxjs';
import { catchError, tap } from 'rxjs/operators';

const httpOptions = {
  headers: new HttpHeaders({ 'Content-Type': 'application/json' })
};

@Injectable({ providedIn: 'root' })
export class DataService {

  dataUrl = '../assets/richList.json';  // URL to data api

  constructor(private http: HttpClient) { }

  /** GET data from the server */
  getData(): Observable<any> {
    return this.http.get<any>(this.dataUrl)
      .pipe(
        tap(data => this.log('fetched data')),
        catchError(this.handleError('getData', []))
      );
  }

  /**
   * Handle Http operation that failed.
   * Let the app continue.
   * @param operation - name of the operation that failed
   * @param result - optional value to return as the observable result
   */
  private handleError<T>(operation = 'operation', result?: T) {
    return (error: any): Observable<T> => {

      // TODO: send the error to remote logging infrastructure
      console.error(error); // log to console instead

      // TODO: better job of transforming error for user consumption
      this.log(`${operation} failed: ${error.message}`);

      // Let the app keep running by returning an empty result.
      return of(result as T);
    };
  }

  // TODO: better job of logging app status
  private log(message: string) {
    console.log(`DataService: ${message}`);
  }
}
