import { TestBed, inject } from '@angular/core/testing';

import { SearchService } from './search.service';
describe('SearchService', () => {
  let searchService: SearchService;
  const testData = [
    {
      'rank': 2,
      'name': 'Rupert Murdoch',
      'netWorth': 14000000000,
      'age': '84',
      'country': 'Australia'
    },
    {
      'rank': 3,
      'name': 'Donald Newhouse',
      'netWorth': 8400000000,
      'age': '85',
      'country': 'United States'
    }
  ];

  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [SearchService]
    });

    searchService = TestBed.get(SearchService);
  });

  /// SearchService method tests begin ///

  describe('#filter', () => {
    beforeEach(() => {
      searchService = TestBed.get(SearchService);
    });

    it('should filter array of objects by values without a key', () => {
      const result = searchService.filter(testData, 'rupert', '', 'netWorth');
      expect(JSON.stringify(result)).toEqual(JSON.stringify([testData[0]]), 'should return John Walton entry');
    });

    it('should filter array of objects by values with a key', () => {
      const result = searchService.filter(testData, 'united states', 'country', '');
      expect(JSON.stringify(result)).toEqual(JSON.stringify([testData[1]]), 'should return John Walton entry');
    });
  });

  describe('#sort', () => {
    beforeEach(() => {
      searchService = TestBed.get(SearchService);
    });

    it('should sort array of objects by age ascendingly', () => {
      const result = searchService.sort(testData, 'age');
      expect(JSON.stringify(result)).toEqual(JSON.stringify(testData), 'should return John Walton entry');
    });

    it('should sort array of objects by name ascendingly', () => {
      const result = searchService.sort(testData, 'name');
      expect(JSON.stringify(result)).toEqual(JSON.stringify(testData.reverse()), 'should return John Walton entry');
    });
  });
});
